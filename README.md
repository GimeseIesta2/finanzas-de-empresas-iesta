<div align="center">

![Logo](grafi/logo_inst_80.png)

</div>


# Estrategias financieras, comerciales y laborales frente a la pandemia de COVID-19, una muestra de Empresas Uruguayas.

El trabajo realizado y recopilado en este repositorio surge de una investigación que tiene como objetivo recabar información respecto al impacto que ha tenido la pandemia del COVID 19 en una muestra de empresas uruguayas. Esto se realiza a partir de la recolección de datos sobre estrategias comerciales, financieras y laborales que llevaron adelante las empresas frente a una situación muy particular como es el COVID.

El análisis de los resultados pretende evidenciar como las empresas de la muestra tuvieron que controlar sus variables económicas-financieras para hacer frente a la incertidumbre de la situación. A su vez implementar en muchos casos la opción de *trabajo remoto* para poder continuar con sus actividades, los cambios tecnológicos que debieron realizar para atender la demanda de productos o servicios. Por último, se analizó el impacto que tuvieron las medidas de gobierno para que las empresas pudieran seguir funcionando y afrontar la difícil situación generada por la pandemia. 

Como metodología de análisis se utilizó principalmente el método de clustering *k-modes* de perfil modal, para la determinación de tipologías de empresas en base a las variables estudiadas, de forma de obtener perfiles claramente diferenciados. Otra enfoque que se le dió al trabajo, más allá de la temática *financiera* y con una óptica mas *estadística*, es poder estudiar las propiedades de este algoritmo para datos categóricos bajo distintos escenarios.

Ambos enfoques fueron y serán presentados en distintas instancias de intercambio académico.

## Shiny App : Impacto Covid-19 en Empresas Uruguayas
[![](https://img.shields.io/badge/Shiny-shinyapps.io-blue?style=flat&labelColor=white&logo=RStudio&logoColor=blue)](https://diegoarare.shinyapps.io/AppKmodes_v3/)

Se creó una aplicación Shiny para poder realizar el análisis (exploratorio y de clustering) de los distintos bloques de información de una manera simple.
Se puede acceder a la aplicación Shiny a través de este [link](https://diegoarare.shinyapps.io/AppKmodes_v3/). 

## Documentos

- [ESTUDIO DE ALGUNAS PROPIEDADES DEL MÉTODO DE CLUSTERING K-MODES MEDIANTE REMUESTREO, APLICADO A LA TOMA DE DECISIONES EMPRESARIALES EN EL MARCO DEL COVID EN URUGUAY](https://osf.io/rwybh/) Álvarez-Vaz, R., PhD, Araujo, D., & de la Vega, M. (2022, Octubre).

## Presentaciones

- [Seminario de Investigación DCA](https://www.youtube.com/watch?v=sfDEi25DAQM&t=1249s) Facultad de Ciencias Económicas y de Administración, Universidad de la República (2021)
- [XV Semana internacional de la Estadística y Probabilidad FCFM-BUAP](https://www.fcfm.buap.mx/SIEP/2022/siep.html). Puebla, México (2022).
- [VI Jornadas Argentinas de Econometría](https://iiep-baires.econ.uba.ar/actividad/427). IIEP - BAIRES, Universidad de Buenos Aires (2022).
- [LatinR 2022](https://www.youtube.com/watch?v=zGQKYUZrXXU&t=6s) Conferencia Latinoamericana sobre Uso de R en Investigación + Desarrollo (2022).


